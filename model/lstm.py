import tensorflow as tf
import numpy as np

# Network Parameters
num_hidden_1 = 256  # 1st layer num features
num_hidden_2 = 128  # 2nd layer num features (the latent dim)


def get_params():
    """Model params."""
    params = {
        "use_timesteps": True
    }
    return params


def model_fn(x, params):
    feature_size = params.feature_size

    # Construct model
    rnn_layers = [
        tf.nn.rnn_cell.LSTMCell(
          num_units=units,
          forget_bias=1.0,
          activation=tf.nn.tanh
        ) for units in [num_hidden_1, num_hidden_2, num_hidden_1, feature_size]
    ]

    # create a RNN cell composed sequentially of a number of RNNCells
    multi_rnn_cell = tf.nn.rnn_cell.MultiRNNCell(rnn_layers)
    # x       (batches, steps, features)
    # outputs (batches, steps, features)
    outputs, states = tf.nn.dynamic_rnn(
        cell=multi_rnn_cell,
        inputs=x,
        dtype=tf.float32,
        time_major=False
    )
    return outputs
