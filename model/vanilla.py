import tensorflow as tf
import numpy as np

# Network Parameters
num_hidden_1 = 256  # 1st layer num features
num_hidden_2 = 128  # 2nd layer num features (the latent dim)


def get_params():
    """Model params."""
    params = {
        "use_timesteps": False
    }
    return params


def model_fn(x, params):
    feature_size = params.feature_size * params.time_steps

    # Construct model
    encoder_1_output = tf.layers.dense(
        inputs=x,
        units=num_hidden_1,
        activation=tf.nn.tanh
    )
    encoder_2_output = tf.layers.dense(
        inputs=encoder_1_output,
        units=num_hidden_2,
        activation=tf.nn.tanh
    )
    decoder_1_output = tf.layers.dense(
        inputs=encoder_2_output,
        units=num_hidden_1,
        activation=tf.nn.tanh
    )
    decoder_2_output = tf.layers.dense(
        inputs=decoder_1_output,
        units=feature_size,
        activation=tf.nn.tanh
    )

    return decoder_2_output
