from __future__ import division, print_function, absolute_import

import tensorflow as tf
from dataset import openfind
from dataset import mnist
from model import lstm
from model import vanilla

tf.reset_default_graph()

tf.logging.set_verbosity(tf.logging.INFO)

tf.flags.DEFINE_string("dataset",       "openfind",             "Dataset name.")
tf.flags.DEFINE_string("model",         "lstm",                 "Dataset name.")
tf.flags.DEFINE_string("output_dir",    "",                     "Optional output dir.")
tf.flags.DEFINE_string("schedule",      "train_and_evaluate",   "Schedule.")
tf.flags.DEFINE_string("hparams",       "",                     "Hyper parameters.")

tf.flags.DEFINE_integer("save_summary_steps",       10,     "Summary steps.")
tf.flags.DEFINE_integer("save_checkpoints_steps",   10,     "Checkpoint steps.")
tf.flags.DEFINE_integer("max_steps",                150,    "max_stp for training.")
tf.flags.DEFINE_integer("eval_steps",               10,     "Number of eval steps.")
tf.flags.DEFINE_integer("eval_frequency",           10,     "Eval frequency.")

FLAGS = tf.flags.FLAGS

DATASETS = {
    "mnist": mnist,
    "openfind": openfind
}

MODELS = {
    "lstm": lstm,
    "vanilla": vanilla
}

# Training Parameters
HPARAMS = {
    "optimizer": "Adam",
    "learning_rate": 0.01,
    "decay_steps": 100,
    "decay_rate": 0.1,
    "min_eval_frequency": FLAGS.eval_frequency
}

def get_params():
    """Aggregates and returns hyper parameters."""
    dataset_params = DATASETS[FLAGS.dataset].get_params()
    model_params = MODELS[FLAGS.model].get_params()

    hparams = HPARAMS
    hparams.update(dataset_params)
    hparams.update(model_params)

    hparams = tf.contrib.training.HParams(**hparams)
    hparams.parse(FLAGS.hparams)

    return hparams


def make_model_fn():
    def _model_fn(features, labels, mode, params):
        global_step = tf.train.get_or_create_global_step()

        inputs = features['X']

        outputs = MODELS[FLAGS.model].model_fn(inputs, params)

        # Prediction
        y_pred = tf.squeeze(outputs, name="reconstruction")
        # Targets (Labels) are the input data.
        y_true = tf.squeeze(inputs, name="squeeze_input")

        # Define loss and optimizer, minimize the squared error
        loss = tf.losses.mean_squared_error(inputs, outputs)

        if mode == tf.estimator.ModeKeys.TRAIN:
            def _decay(learning_rate, global_step):
                learning_rate = tf.train.exponential_decay(
                    learning_rate=learning_rate,
                    global_step=global_step,
                    decay_steps=params.decay_steps,
                    decay_rate=params.decay_rate
                )
                return learning_rate

            train_op = tf.contrib.layers.optimize_loss(
                loss=loss,
                global_step=global_step,
                learning_rate=params.learning_rate,
                optimizer=params.optimizer,
                learning_rate_decay_fn=_decay
            )
            #optimizer = tf.train.AdamOptimizer(
            #    learning_rate=params.learning_rate,
            #)
            #train_op = optimizer.minimize(
            #    loss=loss,
            #    global_step=global_step
            #)

            estimator_spec = tf.estimator.EstimatorSpec(
                mode=mode,
                loss=loss,
                train_op=train_op
            )

        if mode == tf.estimator.ModeKeys.EVAL:
            eval_metric_ops = {
            }

            estimator_spec = tf.estimator.EstimatorSpec(
                mode=mode,
                loss=loss,
                eval_metric_ops=eval_metric_ops,
            )

        if mode == tf.estimator.ModeKeys.PREDICT:
            # Define prediction
            predictions = {
                "reconstruction": y_pred,
                "reconstruct_error": loss
            }

            # Define export outputs
            export_outputs = {
                'prediction': tf.estimator.export.PredictOutput(predictions)
            }

            estimator_spec = tf.estimator.EstimatorSpec(
                mode=mode,
                predictions=predictions,
                export_outputs=export_outputs
            )

        return estimator_spec

    return _model_fn


def make_input_fn(mode, params):
    _input_fn = DATASETS[FLAGS.dataset].input_fn(mode, params)

    return _input_fn


def main(unused_argv):
    model = FLAGS.model
    dataset = FLAGS.dataset
    model_dir = "outputs/%s/%s" % (model, dataset)
    print("model dir: %s" % (model_dir))

    gpu_options = tf.GPUOptions(
        per_process_gpu_memory_fraction=0.7,
        allow_growth=True
    )

    session_config = tf.ConfigProto(gpu_options=gpu_options)
    session_config.allow_soft_placement = True
    run_config = tf.estimator.RunConfig(
        model_dir=model_dir,
        save_summary_steps=FLAGS.save_summary_steps,
        save_checkpoints_steps=FLAGS.save_checkpoints_steps,
        save_checkpoints_secs=None,
        session_config=session_config
    )

    hparams = get_params()

    estimator = tf.estimator.Estimator(
        model_fn=make_model_fn(),
        config=run_config,
        params=hparams
    )

    train_spec = tf.estimator.TrainSpec(
        input_fn=make_input_fn(tf.estimator.ModeKeys.TRAIN, hparams),
        max_steps=FLAGS.max_steps
    )

    eval_spec = tf.estimator.EvalSpec(
        input_fn=make_input_fn(tf.estimator.ModeKeys.EVAL, hparams),
        steps=FLAGS.eval_steps,
        throttle_secs=60
    )

    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)

    # input
    sequence = tf.placeholder(
        tf.float32,
        shape=[None, hparams.time_steps, hparams.feature_size],
        name='sequence'
    )

    # input receiver
    input_fn = tf.estimator.export.build_raw_serving_input_receiver_fn({
        'X': sequence,
    })

    # export model
    estimator.export_savedmodel(model_dir, input_fn)


if __name__ == "__main__":
    tf.app.run()
