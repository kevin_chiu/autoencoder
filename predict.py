import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import Imputer, StandardScaler


time_steps = 200
feature_size = 682
export_dir = './outputs/lstm/openfind/angus_chang_full_length'


def input_fn(file):
    df = pd.read_csv(file, header=None, dtype=np.float32, delimiter=",")  # [?, 682]

    # convert to numpy array
    X = df.values

    # handle nan
    X[np.isnan(X)] = 0

    # standardiztation
    scaler = StandardScaler()
    X = scaler.fit_transform(X)

    # cut data in exactly lengh of (n * time_steps)
    num_sequence = len(X) // time_steps
    padding_length = num_sequence * time_steps

    pad_data = X[:padding_length]
    all_data = pad_data.reshape((-1, time_steps, feature_size))  # [?, 200, 682]

    return all_data, num_sequence


persons = {
    'angus':    './vector/angus_chang.1.csv',
    'ben':      './vector/ben_ko.1.csv',
    'joan':     './vector/joan_hsu.1.csv',
    'michelle': './vector/michelle_song.1.csv',
    'solos':    './vector/solos_chang.1.csv',
    'yui':      './vector/yui_wu.1.csv'
}

with tf.Session(graph=tf.Graph()) as sess:
    # saved_model load
    tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], export_dir)

    # input
    input = sess.graph.get_tensor_by_name("sequence:0")

    # output
    reconstruct_err = sess.graph.get_tensor_by_name("mean_squared_error/value:0")

    for person in persons:
        print("Start predict '%s' now ..." % (person))
        file = persons[person]
        all_data, num_sequence = input_fn(file)

        for i in range(num_sequence):
            data = all_data[i].reshape(1, 200, 682)
            loss = sess.run(reconstruct_err, feed_dict={input: data})
            print("%3d: %f" % (i, loss))