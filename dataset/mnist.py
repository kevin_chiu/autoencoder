import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data


def get_params():
    """Dataset params."""
    params = {
        "time_steps": 28,
        "feature_size": 28,
        "batch_size": 20
    }
    return params


def input_fn(mode, params):
    time_steps = params.time_steps
    feature_size = params.feature_size
    batch_size = params.batch_size
    use_timesteps = params.use_timesteps

    mnist = input_data.read_data_sets("mnist", one_hot=True)
    train_x = mnist.train.images
    test_x = mnist.test.images

    if mode == tf.estimator.ModeKeys.TRAIN:
        inputs = train_x
    elif mode == tf.estimator.ModeKeys.EVAL:
        inputs = test_x

    if use_timesteps:
        inputs = inputs.reshape(-1, time_steps, feature_size)
    else:
        inputs = inputs.reshape(-1, time_steps * feature_size)

    return tf.estimator.inputs.numpy_input_fn(
        x={'X': inputs},
        batch_size=batch_size,
        shuffle=True
    )