import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import Imputer, StandardScaler


def get_params():
    """Dataset params."""
    params = {
        "time_steps": 200,
        "feature_size": 682
    }
    return params


def get_padding_data(file, timesteps):
    df = pd.read_csv(file, header=None, dtype=np.float32, delimiter=",")  # [?, 682]

    # convert to numpy array
    X = df.values

    # handle nan
    X[np.isnan(X)] = 0

    # standardiztation
    scaler = StandardScaler()
    X = scaler.fit_transform(X)

    num_sequence = len(X) // timesteps
    padding_length = num_sequence * timesteps

    return X[:padding_length], num_sequence


def input_fn(mode, params):
    time_steps = params.time_steps
    feature_size = params.feature_size
    use_timesteps = params.use_timesteps

    pad_data, num_sequence = get_padding_data('vector/yui_wu.1.csv', time_steps)
    all_data = pad_data.reshape((-1, time_steps, feature_size))

    if mode == tf.estimator.ModeKeys.EVAL:
        input_data = all_data[1]
    elif mode == tf.estimator.ModeKeys.TRAIN:
        input_data = all_data

    if use_timesteps:
        sequence = input_data.reshape((-1, time_steps, feature_size))
    else:
        sequence = input_data.reshape((-1, feature_size))

    batch_size = num_sequence if num_sequence < 100 else 100

    return tf.estimator.inputs.numpy_input_fn(
        batch_size=batch_size,
        x={'X': sequence},
        shuffle=False
    )
